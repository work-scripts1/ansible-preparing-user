#!/bin/bash

# Создание пользователя для подключения к хостам. Включая доступ по ssh ключам.
# Пароль для пользователя генерится рандомным образом и состоит из 20 символов
# На удаленной машине залогинится под рутом и запустить вот так:
# curl -s https://devops.org.ru/cu.sh | bash /dev/stdin

# Переменные
USERNAME="devops"
PASSWORD=`tr -cd '[:alnum:]' < /dev/urandom | fold -w20 | head -n1`
HOSTNAME=`hostname -I | cut -d' ' -f1`
# Публичный ключ для файла authorized_keys сгенерированный с помощью linux машины управления
LINUXKEY="ssh-rsa key"
# Публичный ключ для файла authorized_keys сгенерированный с помощью putty
PUTTYKEY="ssh-rsa key"
# Проверяем, под рутом ли запущен скрипт.
if [ "$(whoami)" != 'root' ]; then
echo "Для запуска скрипта вы должны войти под пользователем ROOT! Выходим..."
exit 1;
fi

# Проверяем пользователя, есть ли такой в системе.
if id -u $USERNAME >/dev/null 2>&1;
then
    echo "Пользователь $USERNAME уже есть в системе. Выходим"
    exit 1;
fi

# Создаем пользователя
useradd -m -s /bin/bash "$USERNAME"
echo -e "$PASSWORD\n$PASSWORD" | passwd "$USERNAME"

# Прописываем пользователю SUDO без запроса пароля
echo "$USERNAME     ALL=(ALL)    NOPASSWD: ALL" > /etc/sudoers.d/$USERNAME

# Создаем директорию .ssh и файл авторизации по ключам
mkdir -p /home/$USERNAME/.ssh
echo $LINUXKEY >> /home/$USERNAME/.ssh/authorized_keys
echo $PUTTYKEY >> /home/$USERNAME/.ssh/authorized_keys

# Исправляем прова доступа
chown -R $USERNAME:$USERNAME /home/$USERNAME/.ssh/authorized_keys
chmod -R 0600 /home/$USERNAME/.ssh/authorized_keys

echo ""
echo "Пользователь $USERNAME c паролем: $PASSWORD создан на хосте $HOSTNAME"
